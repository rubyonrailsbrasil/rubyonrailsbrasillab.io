---
title: Pesquisas Oficiais
date: 2016-04-26
tags: Comunidade
layout: post
author: brodock
---

Uma parte importante de se organizar em comunidades é poder trocar informações,
experiências e identificar as boas práticas.

Estamos iniciando um projeto interno pra tentar facilitar a busca de informações
para as dúvidas mais comuns dos novos membros, sobre o que usar em determinadas
situações.

A idéia veio do James Pinto ([@thejamespinto](https://twitter.com/thejamespinto)), que já teve sucesso implantando isso na comunidade
internacional. Ficou definido o seguinte:

* Refazer as pesquisas pelo menos 1 vez por ano
* Compilar os resultados em artigos e publicar aqui no site
* Manter o link para o artigo atualizado em algum lugar de destaque da comunidade

Vamos manter um link no post principal do grupo apontando para essa lista de
pesquisas. Ajudem os novos membros direcionando eles para elas, sem censurar
a discussão e sempre seguindo o MINSWAN (_Matz is nice so we are nice_).

Seguem o link para as pesquisas até o momento:

* [Qual Sistema Operacional você esta usando para desenvolver em Rails?](https://www.facebook.com/groups/rubyonrailsbrasil/permalink/872296962897816/)
* [Qual Sistema Operaciona você não aconselha para desenvolver em Rails?](https://www.facebook.com/groups/rubyonrailsbrasil/permalink/872303349563844/)
* [Windows+Bash (cygwin ou Ubuntu integrado no Windows 10) para desenvolver em Rails](https://www.facebook.com/groups/rubyonrailsbrasil/permalink/872309539563225/)
* [Material recomendado para iniciante](https://www.facebook.com/groups/rubyonrailsbrasil/permalink/869902236470622/)
* [Hospedagem Rails para quem tem experiência](https://www.facebook.com/groups/rubyonrailsbrasil/permalink/870423689751810/)
* [Hospedagem Rails para quem é iniciante](https://www.facebook.com/groups/rubyonrailsbrasil/permalink/870423379751841/)

Compilaremos artigos com os resultados em Junho, mas as pesquisas continuarão abertas até Dezembro.
