/*
* Based on the work of Ryan Patterson
* http://cgamesplay.com/2014/01/25/embedding-charts.html
* https://gist.github.com/CGamesPlay/8628541
*/

(function() {
  // 9
  // 9,999
  // 99 999
  // 99.9
  var MATCH_NUMBER = /[0-9, ]+(\.\d*)?/;
  var CHART_HEIGHT = 400;
  var MAX_COLORS = 16;
  var unique_id = 0;

  function hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
        return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
        return "rgb(" + r + ", " + g + ", " + b + ")";
    }
  }

  function color(n) {
    var sch = new ColorScheme
    sch.from_hex('1feb00').scheme('tetrade').variation('default').distance(1);
    if (n%2 == 0) {
      var color = '#' + sch.colors()[n];
    } else {
      var color = '#' + sch.colors()[MAX_COLORS-n];
    }

    return {
      backgroundColor: hexToRGB(color, 0.8),
      borderColor: hexToRGB(color, 0.9),
      borderWidth: 1,
      pointColor: hexToRGB(color, 1),
      pointborderColor: "#fff"
    };
  }

  function get_data(container) {
    // Fail unless the next element in the DOM is the data table.
    var table = container.nextElementSibling;
    var series_shift = 2;
    var axis_selector = 'td:nth-child(1)';
    var label_selector = 'th:nth-child(N)';
    var series_selector = 'td:nth-child(N)';

    if (container.dataset.series == "rows") {
      series_shift = 1;
      axis_selector = 'th:not(:nth-child(1))';
      label_selector = 'tbody tr:nth-child(N) :first-child';
      series_selector = 'tbody tr:nth-child(N) :not(:first-child)';
    }

    // Assume the columns form series.
    var labels =
    Array.prototype.slice.call(table.querySelectorAll(axis_selector), 0)
    .map(function(e) { return e.textContent; });
    var datasets = [];
    for (var N = 0; N < MAX_COLORS; N++) {
      var series_title =
      table.querySelector(label_selector.replace('N', N + series_shift));
      if (!series_title) {
        break;
      }
      var series_data =
      Array.prototype.slice.call(table.querySelectorAll(series_selector.replace('N', N + series_shift)), 0)
      .map(function(e) {
        var match = MATCH_NUMBER.exec(e.textContent) || [ '' ];
        return parseFloat(match[0].replace(/[^0-9.]/g, ''));
      });
      var series = color(N);
      series.data = series_data;
      series.label = series_title.textContent;
      datasets.push(series);
    }
    return {
      labels: labels,
      datasets: datasets,
      table: table,
    };
  }

  function make_tabs(container, data) {
    var focus_table = container.dataset.focus != "chart";
    var tabs = document.createElement('div');
    tabs.className = "tabs";

    var view_chart = document.createElement('input');
    var view_chart_label = document.createElement('label');
    view_chart.type = 'radio';
    view_chart.name = 'view_' + container.id;
    view_chart.className = view_chart_label.className = "view_chart";
    view_chart.id = view_chart_label.htmlFor = 'view_' + container.id + '_chart';
    view_chart.checked = !focus_table;
    view_chart_label.textContent = "Gráfico";
    container.appendChild(view_chart);
    tabs.appendChild(view_chart_label);

    var view_table = document.createElement('input');
    var view_table_label = document.createElement('label');
    view_table.type = 'radio';
    view_table.name = 'view_' + container.id;
    view_table.className = view_table_label.className = "view_table";
    view_table.id = view_table_label.htmlFor = 'view_' + container.id + '_table';
    view_table.checked = focus_table;
    view_table_label.textContent = "Dados";
    container.appendChild(view_table);
    tabs.appendChild(view_table_label);

    container.appendChild(tabs);
    container.appendChild(data.table);

    return view_chart;
  }

  function make_chart(container) {
    var id = "chart_" + ++unique_id;
    container.id = id;
    container.className = "figure";

    var focus_table = container.dataset.focus != "chart";
    var data = get_data(container);
    var options = {
      scaleFontFamily: 'Roboto',
      scaleFontSize: 14,
      showLegend: true,
    };
    for (var k in container.dataset) {
      options[k] = container.dataset[k];
    }
    var chart_type = container.dataset.type;
    if (!container.dataset.type) {
      chart_type = 'bar';
    }

    var view_chart = make_tabs(container, data);

    var canvas = document.createElement("canvas");
    canvas.width = container.clientWidth;
    canvas.height = Math.min(CHART_HEIGHT, document.body.clientHeight - 20);
    container.appendChild(canvas);

    var rendered = false;
    function render() {
      var new_width = container.clientWidth;
      var new_height = Math.min(CHART_HEIGHT, document.body.clientHeight - 20);

      if (rendered && canvas.width == new_width && canvas.height == new_height) {
        return;
      }
      rendered = true;

      canvas.width = new_width;
      canvas.height = new_height;
      var ctx = canvas.getContext('2d');
      var myChart = new window.Chart(ctx, {
        type: chart_type,
        data: data,
        options: options
      });
    }

    if (!focus_table) {
      render();
    }
    view_chart.addEventListener('change', render, false);
    window.addEventListener('resize', function() {
      if (view_chart.checked) {
        render();
      }
    }, false);
  }

  window.addEventListener('load', function() {
    Array.prototype.slice.call(document.getElementsByTagName("div"), 0)
    .filter(function(e) { return e.dataset.figure === 'chart'; })
    .map(make_chart);
  }, false);
}());
